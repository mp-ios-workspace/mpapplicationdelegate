
#if canImport(UIKit)

import UIKit

public protocol MPApplicationService: UIApplicationDelegate, UNUserNotificationCenterDelegate {}

public extension MPApplicationService {
    var window: UIWindow? {
        UIApplication.shared.delegate?.window ?? nil
    }
}

#endif
