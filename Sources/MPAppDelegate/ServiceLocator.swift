
public protocol ServiceLocating {
    func getService<T>() -> T?
}

public class ServiceLocator: ServiceLocating {
    
    public static let shared = ServiceLocator()
    private lazy var services: [String: Any] = [:]
    
    public func add<T>(service: T) {
        let key = typeName(some: T.self)
        services[key] = service
    }
    
    public func getService<T>() -> T? {
        let key = typeName(some: T.self)
        return services[key] as? T
    }
    
    private func typeName(some: Any) -> String {
        return (some is Any.Type) ? "\(some)" : "\(type(of: some))"
    }
}
