
#if canImport(UIKit)

import UIKit

open class MPApplicationDelegate: UIResponder, UIApplicationDelegate {
    
    open var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
    
    open var services: [MPApplicationService] { return [] }
    
    private lazy var _services: [MPApplicationService] = { services }()
    
}

public extension MPApplicationDelegate {
    
    func application(
        _ application: UIApplication,
        willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil
    ) -> Bool {
        
        var result = false
        for service in _services {
            result = service.application?(
                application,
                willFinishLaunchingWithOptions: launchOptions
            ) ?? false
        }
        
        return result
    }
    
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil
    ) -> Bool {
        
        var result = false
        for service in _services {
            result = service.application?(
                application,
                didFinishLaunchingWithOptions: launchOptions
            ) ?? false
        }
        
        return result
    }
    
    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
    ) -> Bool {
        
        var result = false
        for service in _services {
            result = service.application?(
                app,
                open: url,
                options: options
            ) ?? false
        }
        
        return result
    }
    
    func application(
        _ application: UIApplication,
        continue userActivity: NSUserActivity,
        restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void
    ) -> Bool {
        
        var result = false
        for service in _services {
            result = service.application?(
                application,
                continue: userActivity,
                restorationHandler: restorationHandler
            ) ?? false
        }
        
        return result
    }
    
    func application(
        _ application: UIApplication,
        supportedInterfaceOrientationsFor window: UIWindow?
    ) -> UIInterfaceOrientationMask {
        
        for service in _services {
            if let result = service.application?(
                application,
                supportedInterfaceOrientationsFor: window
            ) {
                return result
            }
        }
        
        return .portrait
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        _services.forEach { $0.applicationWillResignActive?(application) }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        _services.forEach { $0.applicationDidBecomeActive?(application) }
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        _services.forEach { $0.applicationDidEnterBackground?(application) }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        _services.forEach { $0.applicationWillEnterForeground?(application) }
    }
    
}

extension MPApplicationDelegate: UNUserNotificationCenterDelegate {
    
    public func application(
        _ application: UIApplication,
        didFailToRegisterForRemoteNotificationsWithError error: Error
    ) {
        
        for service in _services {
            service.application?(
                application,
                didFailToRegisterForRemoteNotificationsWithError: error
            )
        }
        
    }
    
    public func application(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
    ) {
        
        for service in _services {
            service.application?(
                application,
                didRegisterForRemoteNotificationsWithDeviceToken: deviceToken
            )
        }
        
    }
    
    public func application(
        _ application: UIApplication,
        didReceiveRemoteNotification userInfo: [AnyHashable : Any],
        fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void
    ) {
        
        for service in _services {
            service.application?(
                application,
                didReceiveRemoteNotification: userInfo,
                fetchCompletionHandler: completionHandler
            )
        }
        
    }
    
    public func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void
    ) {
        
        for service in _services {
            service.userNotificationCenter?(
                center,
                didReceive: response,
                withCompletionHandler: completionHandler
            )
        }
        
    }
    
}

#endif
