// swift-tools-version:5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "MPAppDelegate",
    platforms: [
        .iOS(.v10),
        .tvOS(.v10)
    ],
    products: [
        .library(
            name: "MPAppDelegate",
            targets: ["MPAppDelegate"])
    ],
    dependencies: [],
    targets: [
        .target(
            name: "MPAppDelegate",
            dependencies: []),
    ]
)
