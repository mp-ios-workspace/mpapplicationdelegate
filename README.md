# MPApplicationDelegate

## Как установить

1. Xcode
2. File Drop Down menu
3. Swift Packages
4. Add Package Dependency
5. Copy and paste url **https://bitbucket.org/mp-ios-workspace/mpapplicationdelegate**

---
## Как использовать

Библиотека создана для разделения логики класса **AppDelegate** и создания сервисов, которые будут иметь доступ к функциям **ApplicationDelegate**

---
> Пример **AppDelegate**
---

```
@main
class AppDelegate: MPApplicationDelegate {
    
    override var services: [ApplicationService] {
        let userManager = UserManager()
        ServiceLocator.shared.add(service: userManager)
        
        let rootRouter = RootRouter(window: window)
        let rootService = RootService(rootRouter: rootRouter)
        ServiceLocator.shared.add(service: rootService)
        
        return [userManager, rootService]
    }

}
```

---
> Пример **ApplicationService**
---

```
class UserManager: NSObject, MPApplicationService {
    
    var token: String?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {

        if let token = token {
           print(token)
        }

        return true
    }
}
```

---
> Пример **ServiceLocator**
---

```
let someManager: SomeManager? = ServiceLocator.shared.getService()
```
